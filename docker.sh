#!/bin/bash

IMAGE_TAG=registry.gitlab.collabora.com/collabora/gst-build-sdk/gst-build-sdk:main

case "$1" in
  run)
    docker run -it \
    --net=host \
    -v $SYSROOT:$SYSROOT \
    -w $SYSROOT \
    -e SYSROOT \
    -u $( id -u ):$( id -g ) \
    --mount type=tmpfs,destination=/home/user \
    $IMAGE_TAG \
    /bin/bash
    ;;
  build)
    docker build -t $IMAGE_TAG .
    ;;
  *)
    echo "Usage: $0 {run|build}"
    exit 1
esac

