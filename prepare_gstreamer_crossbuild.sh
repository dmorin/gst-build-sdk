#!/bin/bash

if [ -z $SYSROOT ]; then
  echo "Error: \$SYSROOT is not set, set \$SYSROOT with the absolute path to the gst-build-sdk rootfs folder."
  exit 1
fi

git clone https://gitlab.freedesktop.org/gstreamer/gstreamer.git $SYSROOT/home/user/gstreamer
touch $SYSROOT/home/user/gstreamer/cross-file.txt
cat <<EOF >> $SYSROOT/home/user/gstreamer/cross-file.txt
[host_machine]
system = 'linux'
cpu_family = 'aarch64'
cpu = 'aarch64'
endian = 'little'

[properties]
sys_root = '$SYSROOT'
pkg_config_libdir = '$SYSROOT/usr/lib/pkgconfig:$SYSROOT/usr/lib/aarch64-linux-gnu/pkgconfig:$SYSROOT/usr/share/pkgconfig'

[built-in options]
c_args = ['--sysroot=$SYSROOT']
cpp_args = ['--sysroot=$SYSROOT', '-I$SYSROOT/usr/include/c++/10']
c_link_args = ['--sysroot=$SYSROOT']
cpp_link_args = ['--sysroot=$SYSROOT']

[binaries]
c = 'aarch64-linux-gnu-gcc'
cpp = 'aarch64-linux-gnu-g++'
ar = 'aarch64-linux-gnu-ar'
strip = 'aarch64-linux-gnu-strip'
pkgconfig = 'pkg-config'
EOF
