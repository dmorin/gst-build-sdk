#!/bin/bash

set -euo pipefail

usage() {
    echo "Usage $0 {armhf|arm64}"
    exit 1
}

if (( $# != 1 )); then
    usage
fi

if [ -z $SYSROOT ]; then
  echo "Error: \$SYSROOT is not set, set \$SYSROOT with the absolute path to the gst-build-sdk rootfs folder."
  exit 1
fi


case "$1" in
    armhf)
        declare -r arch=arm # or armhf
        declare -r arch_triplet=arm-linux-gnueabihf
        ;;
    arm64)
        declare -r arch=aarch64
        declare -r arch_triplet=aarch64-linux-gnu
        ;;
    *) usage ;;
esac

declare -r builddir=build

export PKG_CONFIG_SYSROOT_DIR="$SYSROOT"
export PKG_CONFIG_LIBDIR="$SYSROOT/usr/lib/pkgconfig:$SYSROOT/usr/lib/${arch_triplet}/pkgconfig:$SYSROOT/usr/share/pkgconfig"

# This configuration is dependent on a fork of FFmpeg
# (https://github.com/jernejsk/FFmpeg), which supports the
# Linux kernel V4L2 request API.
# In case you want to compile main-line FFmpeg, remove the line
# `--enable-v4l2-request`.
# This is the default state as it is a requirement for a lot of kernel
# development work.
./configure \
    --target-os=linux \
    --arch=${arch} \
    --enable-cross-compile \
    --cross-prefix=${arch_triplet}- \
    --sysroot=$SYSROOT \
    --prefix=`pwd`/install \
    --enable-libudev \
    --enable-libdrm \
    --enable-v4l2-request \
    --pkg-config=pkg-config \
    --disable-doc

# Uncomment the following, as needed
#    --enable-debug \
#    --disable-optimizations

make -j`nproc`
make install
