# Cross compilation tools for GStreamer

**Rationale:** Cross compiling GStreamer can be tricky. In certain cases compilers/linkers are not clever enough to pick right header files and/or libraries even though they are given the `--sysroot` option, and they pick those files from the PC host rootfs instead. While investigating the ultimate root cause of this situation is an interesting exercise, a viable workaround is to use the same distribution on a PC and in the sysroot/target rootfs. A systematized way to achieve that is by running the cross toolchain in a Docker container, so that all the developers use the host rootfs which matches the target sysroot even if they use various Linux distributions on their hosts.

**General idea:** One of our preferred ways of running development boards is by having rootfs on an NFS-exported volume, so to combine the two the following approach has been taken: The cross toolchain lives in a Docker container, while the cross compilation sysroot is a directory external to the container but mounted as a Docker volume. This same directory is served by the NFS server. Both the Docker image and the sysroot/rootfs are built using the tools from this repository. It is the contents of this repository that is responsible for making sure the image and the sysroot/rootfs are consistent and contain enough stuff to serve their purpose well.

## What is what of what

* Dockerfile

Contains instructions for Docker to build the host image. Please note that while using the Dockerfile directly is a perfectly valid approach, you can also pull ready-made images from `registry.gitlab.collabora.com/collabora/gst-build-sdk/gst-build-sdk:main`. More on that further into this document. The image is an x86_64 image that runs on your PC host and contains the (sanitized) cross toolchain.

* rootfs.yaml

Contains instructions for [debos](https://github.com/go-debos/debos) to prepare the target (arm64) sysroot/rootfs, which contains both build-time GStreamer dependencies and some essential tools needed to use the generated filesystem as rootfs on your development board. The result is a `.tar.gz` archive, which needs to be uncompressed and then passed to Docker as a volume. More on obtaining and preparing the rootfs later.

* docker.sh

Is a driver file for running Docker in two contexts: for generating a Docker image and for running the image. It is this file that passes a path to the sysroot/rootfs to `docker` invocation. You only need this file, all the rest can be downloaded without running Docker or debos (but of course you can use Dockerfile and rootfs.yaml locally if you want).

* gst-build-sdk-rootfs-arm64.tar.gz

Not a part of this repo, but a product of running debos with `rootfs.yaml`. You can run debos on your own, but you can also download it from build artifacts in our GitLab's CI. More on that later.

## Obtaining docker images from our registry

To download the host docker image:

`docker pull registry.gitlab.collabora.com/collabora/gst-build-sdk/gst-build-sdk:main`

Please note that Docker places the received images in its configured location, e.g. `/var/lib/docker`, so `docker.sh` above will *know* how to access them. Teaching you Docker is beyond the scope of this document, you can go [there](https://docs.docker.com/).

## Obtaining the sysroot/rootfs

Our GitLab provides [a list](https://gitlab.collabora.com/collabora/gst-build-sdk/-/jobs) of CI jobs resulting from commits to this repo. Next to each item in the list there's a "Download artifacts" button which you can use (they come packed in artifacts.zip even if there's just one artifact), alternatively you can go to job details and "Browse" the artifacts there to select just the `.tar.gz`. Either way, you end up having `gst-build-sdk-rootfs-arm64.tar.gz`.

This file contains full rootfs for your target board, which is equipped with enough packages to serve as cross-compilation sysroot at compile time. Since it is a complete rootfs, you need to unpack it as root, otherwise tar won't be able to re-create certain files. The extracted directory path is passed in the `SYSROOT` environment variable to `docker.sh`.

In case you forgot, this is an example invocation which extracts the .tar.gz to a specified directory:

`sudo tar -zxf gst-build-sdk-rootfs-arm64.tar.gz --directory /home/user/your/rootfs/arm64/`

That same extracted directory can then be exported as an NFS volume. Explaining how to do it is beyond the scope of this document; googling around will return many instructions on that.

## Running the builder

The container and the sysroot/rootfs contain enough stuff to successfully cross compile GStreamer.
The intended usage of the Docker image is mainly to invoke the cross compilation toolchain to generate correct binaries.

Clone `gstreamer` and prepare a default meson cross build file (`cross-file.txt`) and run the builder:

```bash
export SYSROOT=/home/user/your/rootfs
./prepare_gstreamer_crossbuild.sh
./docker.sh run
```

The [cross file](https://mesonbuild.com/Cross-compilation.html#defining-the-environment) defines properties of the cross build environment. Here is a small example:
```
[host_machine]
system = 'linux'
cpu_family = 'aarch64'
cpu = 'aarch64'
endian = 'little'

[properties]
sys_root = '$SYSROOT'
pkg_config_libdir = '$SYSROOT/usr/lib/pkgconfig:$SYSROOT/usr/lib/aarch64-linux-gnu/pkgconfig:$SYSROOT/usr/share/pkgconfig'

[built-in options]
c_args = ['--sysroot=$SYSROOT']
cpp_args = ['--sysroot=$SYSROOT', '-I$SYSROOT/usr/include/c++/10']
c_link_args = ['--sysroot=$SYSROOT']
cpp_link_args = ['--sysroot=$SYSROOT']

[binaries]
c = 'aarch64-linux-gnu-gcc'
cpp = 'aarch64-linux-gnu-g++'
ar = 'aarch64-linux-gnu-ar'
strip = 'aarch64-linux-gnu-strip'
pkgconfig = 'pkg-config'
```

Within the builder container enable or disable GStreamer build options (this only serves as an example, execute `meson configure` to get a list of all options):

`meson -Dvaapi=disabled -Dlibav=disabled -Drtsp_server=disabled -Dgst-examples=disabled -Dgst-plugins-good:v4l2-libv4l2=enabled -Dqt5=disabled --cross-file cross-file.txt build`

Followed by:

`ninja -C build`.

## Running the cross compiled GStreamer

### Different options of exposing the cross build files to the target board

#### NFS rootfs

For that you need to boot your development board, with your sysroot/rootfs exported via NFS used as rootfs. Explaining how to boot with NFS rootfs is beyond the scope of this document, but you can look [here](https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt).

#### Mount the folder via network share

Alternatively, you can just mount the cross build environment within the target board. There are multiple ways of mounting folders over the network. Possible options are [NFS mount/NFS
rootfs](https://wiki.archlinux.org/title/NFS)/[SSHFS](https://wiki.archlinux.org/title/SSHFS)/[Syncthing](https://wiki.archlinux.org/title/Syncthing), just to name a few.

*NOTE*: There's a `user` user in the rootfs, with the same password. The user is capable of invoking `sudo`.

For developers the GStreamer mono-repository provides an `uninstalled` environment, which is a way of setting environment variables in such a way, that all the GStreamer stuff is picked from your build directory rather than from system location even if you have distribution GStreamer installed. If the build has been a cross build on a different machine (and that is exactly the case), you need to specify the `--sysroot` argument, which is used to adjust the paths accordingly, so that your GStreamer build cross-compiled on a PC works in the native system. To enter that environment you invoke:

`./gst-env.py --sysroot /home/user/your/rootfs/arm64`

Please note that the `--sysroot` path is as it was in the PC host at compile time. That's not a mistake, even though such a path does not exist in the system on the development board. I'm only guessing, but it seems to me that the purpose of this argument is exactly to *remove* the build-time-specific prefix from certain paths.

Once in the environment:

```
[gst-master] user@gst-build-sdk:~/gstreamer$ which gst-inspect-1.0
/home/user/gst-build/build/subprojects/gstreamer/tools/gst-inspect-1.0

[gst-master] user@gst-build-sdk:~/gstreamer$ gst-inspect-1.0 video4linux2
Plugin Details:
  Name                     video4linux2
  Description              elements for Video 4 Linux
  Filename                 /home/user/gstreamer/build/subprojects/gst-plugins-good/sys/v4l2/libgstvid
eo4linux2.so
  Version                  1.19.0.1
  License                  LGPL
  Source module            gst-plugins-good
  Binary package           GStreamer Good Plug-ins git
  Origin URL               Unknown package origin

  v4l2src: Video (video4linux2) Source
  v4l2sink: Video (video4linux2) Sink
  v4l2radio: Radio (video4linux2) Tuner
  v4l2deviceprovider: Video (video4linux2) Device Provider
  v4l2h264enc: V4L2 H.264 Encoder

  5 features:
  +-- 4 elements
  +-- 1 device providers
```

## Using the container for FFmpeg cross-compilation

You can use this container also for cross-compilation of the [FFmpeg tool](http://ffmpeg.org/), the differences are merely that:
- You don't need a [cross file](https://mesonbuild.com/Cross-compilation.html#defining-the-environment) as it is not build by meson + ninja but with make
- You have to run `./prepare_ffmpeg_crossbuild.sh` instead of `./prepare_gstreamer_crossbuild.sh` to download the tool and set up the cross-build script

### Compiling FFmpeg with more external libraries / different options

The build configuration is controlled by the `cross_build_ffmpeg.sh` script, it is automatically created by the `prepare_ffmpeg_crossbuild.sh` script within the rootfs @ `$SYSROOT/home/user/ffmpeg/cross_build_ffmpeg.sh`.
You can modify the set of options to build by adding or removing flags to the `./configure` command at line 35, to find a list of valid options simply run `./configure --help`.

In case you need to include more external libraries, you will first need to prepare the library for the architecture (e.g. aarch64). You can do that by adding the dependency to the `rootfs.yaml` file, build the rootfs with debos (`debos rootfs.yaml`) and run `./prepare_ffmpeg_crossbuild.sh` again.

### Compiling FFmpeg

Run `./docker.sh run` with the `SYSROOT` environment variable set to the target rootfs. Next you do the following steps *within the container*:
```
cd /home/user/ffmpeg
./cross_build_ffmpeg.sh arm64
```

All the required files are now in the `$SYSROOT/home/user/ffmpeg/install` directory and can be placed onto the target board.

In case you don't use a NFS rootfs:
```
rsync --progress -r $SYSROOT/home/user/ffmpeg/install/* user@ip:/usr/
```

### Using custom kernel headers for compilation

The FFmpeg configurator will use the Linux kernel headers found by the cross-compiler to determine whether a particular hardware accelerator can be activated. When developing on an unstable uABI or when you develop new elements on uABI the default kernel headers are not sufficient.

In this case, you can install the kernel headers onto the Debos rootfs, by navigating to your kernel tree and executing the following command (example for `arm64`):
```
make INSTALL_HDR_PATH=$SYSROOT/usr -j25 ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- headers_install
```

#### The compiler doesn't find the new kernel headers

In some cases, it might happen that the compiler doesn't consider the path from `--sysroot` as the highest priority and will take the default kernel headers.

##### Checking for that issue

You can check if that is the case for you by taking the compilation command that failed from the `ffbuild/config.log` file and append the `-v` parameter. This will cause the compiler to output the list and order of locations it will consider for includes.

##### Workaround

In order to force the compiler to take a different location with the highest priority, simply add the following to the `cross_build_ffmpeg.sh` script (found at `$SYSROOT/home/user/ffmpeg/cross_build_ffmpeg.sh`) at the `./configure` command:
```
     --pkg-config=pkg-config \
+    --extra-cflags="-isystem $SYSROOT/usr/include" \
+    --extra-cxxflags="-isystem $SYSROOT/usr/include"

 # Uncomment the following, as needed
```

As an alternative to that solution, you could also install the new kernel headers into the Docker container to make sure that the headers are taken with the highest priority.
